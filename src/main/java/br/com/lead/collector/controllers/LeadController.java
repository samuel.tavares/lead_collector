package br.com.lead.collector.controllers;

import br.com.lead.collector.DTOs.ResumoDeLeadDTO;
import br.com.lead.collector.models.Lead;
import br.com.lead.collector.services.LeadService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;

@RestController
@RequestMapping("/leads")
public class LeadController {

    @Autowired
    private LeadService leadService;

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public Lead salvarLead(@RequestBody @Valid ResumoDeLeadDTO resumoDeLeadDTO){
        Lead objetoLead = leadService.salvarLead(resumoDeLeadDTO);
        return objetoLead;
    }

    @GetMapping
    public Iterable<Lead> consultarTodosLeads(){
        return leadService.readLead();
    }

    @GetMapping("/{id}")
    public Lead consultarLeadPorId(@PathVariable(name = "id") int id){
        try{
            Lead objetoLead = leadService.buscarLeadporId(id);
            return objetoLead;
        }catch (RuntimeException e){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
        }
    }

    @GetMapping("/detalhes")
    public Lead consultarLeadPorCpf(@RequestParam(name = "cpf") String cpf){
        try{
            Lead objetoLead = leadService.buscarLeadporCpf(cpf);
            return objetoLead;
        }catch (RuntimeException e){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
        }
    }

    @PutMapping("{id}")
    public Lead atualizarLead(@RequestBody @Valid Lead lead, @PathVariable(name = "id") int id){
        try{
            Lead objetoLead = leadService.atualizarLead(id, lead);
            return objetoLead;
        }catch (RuntimeException e){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
        }
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deletarLead(@PathVariable(name = "id") int id){
        try{
            leadService.deletarLead(id);
        }catch (RuntimeException e){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
        }
    }
}
