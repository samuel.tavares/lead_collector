package br.com.lead.collector.DTOs;

import br.com.lead.collector.models.Lead;
import org.hibernate.validator.constraints.br.CPF;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.List;

public class ResumoDeLeadDTO {

    @NotNull(message = "Nome é obrigatório")
    @NotBlank(message = "Nome não pode ser vazio")
    @Size(min = 3, message = "Mínimo 3 caracteres")
    private String nome;

    @NotNull(message = "CPF é obrigatório")
    @NotBlank(message = "CPF não pode ser vazio")
    @CPF(message = "CPF inválido")
    private String cpf;

    @Email(message = "Email inválido")
    private String email;
    private String telefone;

    @NotNull(message = "Obrigatorio produtos vinculados")
    private List<IdProdutoDTO> idProdutoDto;

    public ResumoDeLeadDTO() {
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getCpf() {
        return cpf;
    }

    public void setCpf(String cpf) {
        this.cpf = cpf;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getTelefone() {
        return telefone;
    }

    public void setTelefone(String telefone) {
        this.telefone = telefone;
    }

    public List<IdProdutoDTO> getIdProdutoDto() {
        return idProdutoDto;
    }

    public void setIdProdutoDto(List<IdProdutoDTO> idProdutoDto) {
        this.idProdutoDto = idProdutoDto;
    }

    public Lead converteDTOparaLead(ResumoDeLeadDTO resumoDeLeadDTO){
        Lead lead = new Lead();
        lead.setCpf(resumoDeLeadDTO.getCpf());
        lead.setNome(resumoDeLeadDTO.getNome());
        lead.setEmail(resumoDeLeadDTO.getEmail());
        lead.setTelefone(resumoDeLeadDTO.getTelefone());

        return lead;
    }
}
