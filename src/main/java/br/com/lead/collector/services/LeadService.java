package br.com.lead.collector.services;

import br.com.lead.collector.DTOs.IdProdutoDTO;
import br.com.lead.collector.DTOs.ResumoDeLeadDTO;
import br.com.lead.collector.models.Lead;
import br.com.lead.collector.models.Produto;
import br.com.lead.collector.repositories.LeadRepository;
import br.com.lead.collector.repositories.ProdutoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class LeadService {

    @Autowired
    private LeadRepository leadRepository;

    @Autowired
    private ProdutoRepository produtoRepository;

    public Lead salvarLead(ResumoDeLeadDTO resumoDeLeadDTO){
        Lead lead = resumoDeLeadDTO.converteDTOparaLead(resumoDeLeadDTO);

        List<Integer> idDeProdutos = new ArrayList<>();

        for (IdProdutoDTO idprodutoDTO: resumoDeLeadDTO.getIdProdutoDto()) {
            int id = idprodutoDTO.getId();
            idDeProdutos.add(id);
        }

        Iterable<Produto> produtos = produtoRepository.findAllById(idDeProdutos);

        lead.setProdutos((List<Produto>) produtos);

        Lead objetoLead = leadRepository.save(lead);
        return objetoLead;
    }

    public Iterable<Lead> readLead (){
        return leadRepository.findAll();
    }

    public Lead buscarLeadporId (int id){
        Optional<Lead> leadOptional = leadRepository.findById(id);
        if (leadOptional.isPresent()){
            Lead leadResponse = leadOptional.get();
            return leadResponse;
        }else{
            throw new RuntimeException("Lead não encontrado");
        }
    }

    public Lead buscarLeadporCpf (String cpf){
        Lead lead = leadRepository.findFirstByCpf(cpf);
        if (lead != null){
            return lead;
        }else{
            throw new RuntimeException("Lead não encontrado");
        }
    }

    public Lead atualizarLead(int id, Lead lead){
        Lead leadDB = buscarLeadporId(id);
        lead.setId(leadDB.getId());
        return leadRepository.save(lead);
    }

    public void deletarLead(int id){
        if(leadRepository.existsById(id)) {
            leadRepository.deleteById(id);
        }else{
            throw new RuntimeException("Registro não encontrado");
        }
    }
}
