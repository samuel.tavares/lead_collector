package br.com.lead.collector.services;

import br.com.lead.collector.models.Produto;
import br.com.lead.collector.repositories.ProdutoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import javax.validation.Valid;
import java.util.Optional;

@Service
public class ProdutoService {

    @Autowired
    private ProdutoRepository produtoRepository;

    public Produto salvarProduto (Produto produto) {
        Produto objetoProduto = produtoRepository.save(produto);
        return objetoProduto;
    }

    public Iterable<Produto> buscarTodosProdutos(){
        return produtoRepository.findAll();
    }

    public Produto buscarPorId(int id){
        Optional<Produto> optionalProduto = produtoRepository.findById(id);
        if(optionalProduto.isPresent()){
            Produto objetoProduto = optionalProduto.get();
            return objetoProduto;
        }else{
            throw new RuntimeException("Produto não encontrado");
        }
    }

    public Produto alterarPorId(int id, Produto produto){
        Produto produtoDB = buscarPorId(id);
        produto.setId(produtoDB.getId());
        return produtoRepository.save(produto);
    }

    public void deletarProduto(int id){
        if(produtoRepository.existsById(id)){
             produtoRepository.delete(buscarPorId(id));
        }else{
            throw new RuntimeException("Produto não encontrado");
        }

    }
}
