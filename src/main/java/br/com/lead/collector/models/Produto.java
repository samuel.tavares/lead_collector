package br.com.lead.collector.models;

import org.springframework.format.annotation.NumberFormat;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.validation.constraints.DecimalMax;
import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Entity
public class Produto {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @NotBlank(message = "Nome não pode ser em branco")
    @NotNull(message = "Nome Obrigatório")
    private String nome;

    @NotBlank(message = "Descrição não pode ser em branco")
    @NotNull(message = "Descrição Obrigatório")
    private String descricao;

    @NotNull(message = "Preço Obrigatório")
    @NumberFormat(style = NumberFormat.Style.CURRENCY)
    @DecimalMin("1")
    private double preco;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descrcao) {
        this.descricao = descrcao;
    }

    public double getPreco() {
        return preco;
    }

    public void setPreco(double preco) {
        this.preco = preco;
    }
}
